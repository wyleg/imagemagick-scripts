#!/bin/bash

# tool for making PDX texture maps from PBR
# uses Quixel Mixer naming style

function convert_to_dds() {
	# compression=dxt5
	local name

	if ! [ -z "$2" ];
	then
		compression=$2
	else
		compression="dxt5"
	fi
	
	name=${1%%.png}
	convert ${name}.png -define dds:compression="$compression" -define dds:mipmaps=0 ${name}.dds
}

function create_specular() {

	# Specular RGBA:
	# R - Empire color mask
	# G - Specular (almost always 50% gray)
	# B - Metalness
	# A - Roughness inverse (Gloss)

	local name channels colormask_file
	name=${1%%_Normal.png}
	
	# prepare inverted roughness map
	convert ${name}_Roughness.png -negate rough_inverted.png

	# empire color mask
	if [ -f "${name}_ECM.png" ] ;
	then
		colormask_file="${name}_ECM.png"
		channels=RGB
	else
		colormask_file=""
		channels=GB
	fi

	# combine maps into rgb file
	convert $colormask_file ${name}_Specular.png ${name}_Metalness.png -background black -channel $channels -combine ${name}_spec_pdx_wip.png

	# add inverted roughness as alpha channel
	convert ${name}_spec_pdx_wip.png rough_inverted.png -compose copy-opacity -composite ${name}_spec_pdx.png
	
	convert_to_dds ${name}_spec_pdx.png
	mv ${name}_spec_pdx.dds ${name}_Specular.dds && printf "Created ${name}_Specular.dds\n"

	# CLEANUP
	rm ${name}_spec_pdx*png rough_inverted.png

}

function create_normal() {

	# Normal RGBA:
	# R - Normal Red/unused
	# G - Normal Red
	# B - Emissive
	# A - Normal Green

	local name channels emissive_file
	name=${1%%_Normal.png}

	# recombine a regular normal map into PDX weird ass format
	# split normal map into channels
	convert ${name}_Normal.png -channel R -separate normal_red.png
	convert ${name}_Normal.png -channel G -separate normal_green.png

	# emissive
	if [ -f "${name}_Emissive.png" ] ;
	then
		emissive_file="${name}_Emissive.png"
		channels=RGB
	else
		emissive_file=""
		channels=RG
	fi

	# combine PDX normal map
	convert normal_red.png normal_red.png $emissive_file -background black -channel $channels -combine ${name}_nm_pdx_wip.png
	convert ${name}_nm_pdx_wip.png normal_green.png -compose copy-opacity -composite ${name}_nm_pdx.png

	convert_to_dds ${name}_nm_pdx.png
	mv ${name}_nm_pdx.dds ${name}_Normal.dds && printf "Created ${name}_Normal.dds\n"

	# CLEANUP
	rm normal_red.png normal_green.png ${name}_nm_pdx*png

}

function combine_rgba() {

	# Combine separated greyscale images into one RGBA PNG

	drop_layer="$2"
	if ! [ -z "$drop_layer" ] && [[ $drop_layer != "alpha" ]] ;
	then
		convert "${1}_channel_${drop_layer}.png" -level 100x100% "${1}_channel_${drop_layer}.png"
	fi

	convert ${1}_channel_red.png ${1}_channel_green.png ${1}_channel_blue.png -background black -channel RGB -combine ${1}_wip.png

	if ! [[ $drop_layer == "alpha" ]] ; then
		convert ${1}_wip.png ${1}_channel_alpha.png -compose copy-opacity -composite ${1}_combined.png && printf "Created ${1}_combined.png\n"
	else
		mv ${1}_wip.png ${1}_combined.png && printf "Created ${1}_combined.png\n"
	fi

	# CLEANUP
	rm ${1}_wip.png 2>/dev/null
}

function split_rgba() {

	# Split RGBA image into separate greyscale images 

	local name
	name=${1%%.*}
	convert $1 -channel R -separate ${name}_channel_red.png
	convert $1 -channel G -separate ${name}_channel_green.png
	convert $1 -channel B -separate ${name}_channel_blue.png
	convert $1 -channel Alpha -separate ${name}_channel_alpha.png
}

function cut_middle_layers() {
	filename="$1"
	local mid_level_range="$2"
	let lowlevel="50-mid_level_range/2"
	let highlevel="50+mid_level_range/2"

	convert "$filename" -level 0x${lowlevel}% -define png:compression-level=9 out.png
	convert "$filename" -level ${highlevel}x100% -define png:compression-level=9 out2.png 
	composite -blend 50 out.png out2.png -define png:compression-level=9 "$filename"

	rm out.png out2.png
}

function iron_normal_map() {

	normal_map_filename="$1"
	dds_compression="dxt5"

	if ! [ -z "$2" ] ; then
		mid_level_range="$2"
	else
		mid_level_range=20
	fi

	printf "Mid level range = $mid_level_range\n"

	normal_map_name=${normal_map_filename%%.dds}

	split_rgba $normal_map_filename

	for i in red green alpha ;
	do
		cut_middle_layers "${normal_map_name}_channel_${i}.png" $mid_level_range
	done

	combine_rgba ${normal_map_name}
	mv ${normal_map_name}_combined.png ${normal_map_name}.png
	convert_to_dds ${normal_map_name}.png $dds_compression
	printf "Saved $normal_map_filename\n"

	rm ${normal_map_name}_channel* ${normal_map_name}.png
}

function eaw_to_pdx_fullset() {

	eaw_map_name="$1"

	eaw_diffuse_map_filename="${eaw_map_name}.dds"
	eaw_normal_map_filename="${eaw_map_name}_B.dds"
	eaw_normal_map_name="${eaw_map_name}_B"
	dds_compression="dxt5"
	pdxdirname="pdx_texture_set"

	if ! [ -z "$2" ] ; then
		mid_level_range="$2"
	else
		mid_level_range=20
	fi

	mkdir -p "$pdxdirname"

	# diffuse map

	split_rgba $eaw_diffuse_map_filename
	combine_rgba $eaw_map_name alpha
	mv ${eaw_map_name}_combined.png ${pdxdirname}/${eaw_map_name}_Diffuse.png
	convert_to_dds ${pdxdirname}/${eaw_map_name}_Diffuse.png $dds_compression
	printf "Created ${pdxdirname}/${eaw_map_name}_Diffuse.dds\n"

	# normal map

	split_rgba $eaw_normal_map_filename

	if ! [[ $mid_level_range == 0 ]];
	then
		for i in red green ;
		do
			cut_middle_layers "${eaw_normal_map_name}_channel_${i}.png" $mid_level_range
		done
	fi

	mv "${eaw_normal_map_name}_channel_green.png" "${eaw_normal_map_name}_channel_alpha.png"
	cp "${eaw_normal_map_name}_channel_red.png" "${eaw_normal_map_name}_channel_green.png"
	# copy emission layer from EAW diffuse map
	cp "${eaw_map_name}_channel_alpha.png" "${eaw_normal_map_name}_channel_blue.png"

	#convert "${eaw_normal_map_name}_channel_blue.png" -level 100x100% "${eaw_normal_map_name}_channel_blue.png"

	combine_rgba "${eaw_normal_map_name}"
	mv "${eaw_normal_map_name}_combined.png" ${pdxdirname}/${eaw_map_name}_Normal.png

	convert_to_dds ${pdxdirname}/${eaw_map_name}_Normal.png $dds_compression
	printf "Created ${pdxdirname}/${eaw_normal_map_name}_Diffuse.dds\n"

	# CLEANUP
	rm ${eaw_normal_map_name}_channel* ${eaw_map_name}_channel* ${pdxdirname}/*png

}

while (( "$#" )); do
  case "$1" in
    spec)
      create_specular $2
      break
      ;;  
    nm)
      create_normal $2
      break
      ;;  
    split)
      split_rgba $2
      break
      ;;  
    cmb)
      combine_rgba $2 $3
      break
      ;;  
    todds)
      convert_to_dds $2 $3
      break
      ;;
    set)
      convert_to_dds $2_Diffuse.png
      create_normal $2
      create_specular $2 
      break
      ;;
	iron)
	  iron_normal_map $2 $3 $4
	  break
	  ;;
	eawpdx)
	  eaw_to_pdx_fullset $2 $3
	  break
	  ;;
    help)
      printf "\nCommands:\n\n\
  spec name - create PDX specular map from Quixel exported maps\n\
    R: name_ECM.png - empire color mask\n\
    G: name_Specular.png\n\
    B: name_Metallness.png\n\
    A: name_Roughness.png (will be inverted)\n\
\n\
  nm name - create PDX normal map from Quixel exported maps\n\
    name_Normal.png\n\
    name_Emissive.png\n\
    R: Normal Red/unused\n\
    G: Normal Red\n\
    B: Emissive\n\
    A: Normal Green\n\
\n\
  set name - create a full PDX texture set in .dds\n\
    Diffuse, Specular, Normal\n\
\n\
  split filename - split texture into separate RGBA channels\n\
\n\
  cmb name [drop_layer] - recombine channels in separate files into one RGBA texture\n\
\n\
  todds filename [compression] - convert file to dds\n\
\n\
  iron filename [middle_layer_range] - smooth out the middle levels in PDX normal map, preserving extreme vaules\n\
\n\
  eawpdx name [middle_layer_range] - convert Empire At War texture set to PDX texture set\n\n"
      break
      ;;
    *)
      exit 0
      ;;
  esac
done
